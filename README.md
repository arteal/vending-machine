# README
---
- Ruby Version is in `.ruby-version`
- I used RVM so there is a `.ruby-gemset`
- should be fairly straight forward to run:
  - bundle install all the gems
  - `rails db:setup`
  - `rails s`
  - go to localhost:3000