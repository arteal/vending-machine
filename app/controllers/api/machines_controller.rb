class Api::MachinesController < ActionController::API
  def index
    render json: all_machines
  end

  def update_goods
    good = Good.find(params[:id])
    good.update goods_params
    render json: all_machines
  end

  private

  def all_machines
    Machine.all.to_json(include: [:goods])
  end

  def goods_params
    params.permit(:quantity)
  end
end
