import Vue from 'vue'

import Home from './home'
import Money from './money'
import Navigation from './navigation'

Vue.component('home', Home)
Vue.component('money', Money)

Vue.component('navigation', Navigation)