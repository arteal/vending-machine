import Vue from 'vue'
import Router from 'vue-router'
import Home from './general/home'
import Pad from './moneys/pad'

import * as moneys_components from './moneys/moneys_components'
import * as goods_components from './goods/goods_components'
import * as general_components from './general/general_components'

Vue.use(Router)

const routes = [
  {path: '/', component: Home},
  {path: '/moneys', component: Pad}
]

const router = new Router({
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});

export default router