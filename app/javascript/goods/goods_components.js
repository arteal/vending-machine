import Vue from 'vue';

import Index from './index'
import Show from './show'
import Cart from './cart'

Vue.component('goods-index', Index)
Vue.component('goods-show', Show)
Vue.component('goods-cart', Cart)