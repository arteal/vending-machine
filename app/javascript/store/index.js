import Vue from 'vue'
import Vuex from 'vuex'

import { api } from '../http'

Vue.use(Vuex)

const state = {
  money: 0.0,
  drawer: false,
  machine: 1,
  machines: [],
  cart: [],
  filter: {
    category: '',
    terms: []
  }
}

const actions = {
  toggleDrawer({ commit }, show){ commit('toggleDrawer', show) },
  setMoney({ commit }, money){ commit('setMoney', money) },
  setCategoryFilter({ commit }, category) { commit('setCategoryFilter', category) },
  getMachines({ commit }){
    api.get('machines').then(response => {
      commit('setMachines', response.body)
    })
  },
  addGoodToCart({ commit }, good){ commit('addGoodToCart', good)},
  removeGoodFromCart({ commit }, good) { commit ('removeGoodFromCart', good)},
  clearCart({ commit }) { commit('clearCart')},
  updateGood({ commit }, good) {
    api.put(`machines/${good.machine_id}/update_goods/`, good).then( response => {
      commit('setMachines', response.body)
    })
  }
}

function getCurrentGoods(state) {
  if(!state.machines || state.machines.length === 0)
    return []

  return state.machines.find((m) => {
    return m.id === state.machine
  }).goods
}

const getters = {
  getGoods(state) {
    let goods = getCurrentGoods(state)

    if(state.filter.category.length !== 0) {
      goods = goods.filter((g) => {
        return g.category === state.filter.category
      })
    }

    // Search some stuff
    if(state.filter.terms.length !== 0) { }

    return goods
  },
  getCategories(state) {
    return [...new Set(getCurrentGoods(state).map((g) => g.category))]
  }
}

const mutations = {
  toggleDrawer(state, show) { state.drawer = show },
  setMoney(state, money) { state.money = money },
  setMachines(state, machines) { state.machines = machines },
  setCategoryFilter(state, category) {
    state.filter = {
      ...state.filter,
      category
    }
  },
  addGoodToCart(state, good) {
    state.cart.push(good)
  },
  removeGoodFromCart(state, good) {
    let ind = state.cart.indexOf(good)
    state.cart.splice(ind, 1)
  },
  clearCart(state) {
    state.cart = []
  },
}

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})