# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

machine = Machine.create(
  location: 'Charlotte, NC'
)
snacks = [
  "Drake's Apple Fruit Pie",
  'Austin Cheese Crackers with Cheddar Cheese',
  'Pop-Tarts Frosted Strawberry',
  'Doritos',
  'Skittles',
  'Cheez-It Baked Snack Crackers (27)',
  'Ruffles Original',
  'Cheetos Crunchy',
  'Twix',
  '3 Musketeers'
]

drinks = [
  'Coke',
  'Pepsi',
  'Mountain Dew'
]

goods = {
  snacks: snacks,
  drinks: drinks
}

goods.each do |category, items|
  items.each_with_index do |item, index|
    cost = (rand * 5.0).round(2)
    Good.create(
      category: category,
      name: item,
      machine: machine,
      code: "#{category[0..2]}#{index}",
      cost: cost,
      quantity: 10
    )
  end
end
