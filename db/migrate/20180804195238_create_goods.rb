class CreateGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :goods do |t|
      t.string :category
      t.references :machine, foreign_key: true
      t.float :cost
      t.string :name
      t.integer :quantity

      t.timestamps
    end
  end
end
