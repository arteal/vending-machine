class AddCodeToGoods < ActiveRecord::Migration[5.2]
  def change
    add_column :goods, :code, :string
  end
end
