Rails.application.routes.draw do
  namespace :api do
    resources :machines, only: [:index] do
      put 'update_goods'
    end
  end

  root to: 'static#index'
end
